//
//  ViewAnimationHelper.m
//  EventQuest
//
//  Created by Filipe Patrício on 24/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "ViewAnimationHelper.h"

@implementation ViewAnimationHelper

+(CABasicAnimation*)appearAnimation
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    animation.fromValue = [NSNumber numberWithFloat:1.00];
    animation.toValue = [NSNumber numberWithFloat:1.20];
    animation.beginTime = CACurrentMediaTime()+0.30;
    animation.duration = 0.20;
    animation.autoreverses = YES;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fillMode = kCAFillModeForwards;
    
    return animation;
}

@end
