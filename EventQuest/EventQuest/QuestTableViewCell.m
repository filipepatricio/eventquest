//
//  QuestTableViewCell.m
//  EventQuest
//
//  Created by Filipe Patrício on 22/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestTableViewCell.h"

@implementation QuestTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
