//
//  QuestModel.h
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface QuestModel : PFObject <PFSubclassing>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *info;
@property (strong, nonatomic) NSString *sponsor;
@property (strong, nonatomic) NSString *imageURLString;
@property (assign, nonatomic) NSUInteger minimumChallengesRequiredToFinish;
@property (strong, nonatomic) NSString *prize;
@property (strong, nonatomic) NSString *prizeDeliveryInstructions;
@property (strong, nonatomic) NSArray *challenges;                  //Of ChallengeModel

+ (NSString *)parseClassName;

@end
