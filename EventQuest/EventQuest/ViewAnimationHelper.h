//
//  ViewAnimationHelper.h
//  EventQuest
//
//  Created by Filipe Patrício on 24/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ViewAnimationHelper : NSObject
+(CABasicAnimation*)appearAnimation;
@end
