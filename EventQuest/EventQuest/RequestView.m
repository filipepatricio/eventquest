//
//  RequestView.m
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "RequestView.h"
#import "ViewAnimationHelper.h"

@interface RequestView ()
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *refuseButton;

@end

@implementation RequestView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

-(void)awakeFromNib
{    
    self.acceptButton.layer.cornerRadius = self.acceptButton.frame.size.width/2;
    self.acceptButton.layer.borderWidth = 2;
    self.acceptButton.layer.borderColor = [UIColor colorWithRed:0.0/255.0 green:127.0/255.0 blue:38.0/255.0 alpha:1.0].CGColor;
    
    self.refuseButton.layer.cornerRadius = self.refuseButton.frame.size.width/2;
    self.refuseButton.layer.borderWidth = 2;
    self.refuseButton.layer.borderColor = [UIColor colorWithRed:171.0/255.0 green:16.0/255.0 blue:14.0/255.0 alpha:1.0].CGColor;
}


- (void)drawRect:(CGRect)rect
{
    [self.layer addAnimation:[ViewAnimationHelper appearAnimation] forKey:@"scale"];
}

- (IBAction)actionAccept:(id)sender
{
    [self.delegate didAcceptChallenge:YES];
}

- (IBAction)actionRefuse:(id)sender
{

    //TODO: ask if user's is sure that want to cancel
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wait"
                                                    message:@"Are you sure you want to refuse this challenge?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
                [self.delegate didAcceptChallenge:NO];
            break;
        default:
            break;
    }
}
@end
