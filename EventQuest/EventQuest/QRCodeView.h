//
//  QRCodeView.h
//  EventQuest
//
//  Created by Filipe Patrício on 19/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QRCodeView;

@protocol QRCodeScannerDelegate <NSObject>
@required
-(void)qrcodeView:(QRCodeView*)qrcodeView didReadQRCodeWithString:(NSString*)detectionString;
@optional
-(void)qrcodeView:(QRCodeView*)qrcodeView didCloseScanner:(BOOL)isScannerClosed;
@end

@interface QRCodeView : UIView
@property (weak, nonatomic) id<QRCodeScannerDelegate>delegate;
- (void)removeQrcodeView;
@end
