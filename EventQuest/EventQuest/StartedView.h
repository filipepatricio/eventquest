//
//  StartedView.h
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChallengeStartedDelegate <NSObject>
@required
-(void)verifyWithPhoto;
-(void)didOver;
@end

@interface StartedView : UIView
@property (weak, nonatomic) id<ChallengeStartedDelegate> delegate;
@property (weak, nonatomic) NSDate *dateToFinish;
@property (weak, nonatomic) NSTimer *timerToUpdateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
