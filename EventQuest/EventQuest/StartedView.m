//
//  StartedView.m
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "StartedView.h"
#import "NSDate+Utilities.h"
#import "ViewAnimationHelper.h"

@interface StartedView ()<UIAlertViewDelegate>

@end

@implementation StartedView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.photoButton.layer.cornerRadius =  self.photoButton.frame.size.width/2;
    self.photoButton.layer.borderWidth = 2;
    self.photoButton.layer.borderColor = [UIColor grayColor].CGColor;
    
    NSTimeInterval timeLeft = [self getTimeLeft];
    if(timeLeft > 0)
    {
        self.timerToUpdateTimeLabel = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(updateTimeLabel) userInfo:nil repeats:YES];
        [self.timerToUpdateTimeLabel fire];
    }
    
    [self.layer addAnimation:[ViewAnimationHelper appearAnimation] forKey:@"scale"];
    
}

- (NSTimeInterval)getTimeLeft
{
    NSDate *now = [NSDate date];
    NSTimeInterval timeLeft = [self.dateToFinish timeIntervalSinceDate:now];
    return timeLeft;
}

-(void)updateTimeLabel
{
    NSTimeInterval timeLeft = [self getTimeLeft];
    if(timeLeft <= 0)
    {
        [self.timerToUpdateTimeLabel invalidate];
        self.timerToUpdateTimeLabel = nil;
        [self.delegate didOver];
    }

    NSDictionary *timemap = [NSDate createTimemapForSeconds:timeLeft];
    self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",
                           [timemap[@"h"] longValue],
                           [timemap[@"m"] longValue],
                           [timemap[@"s"] longValue]];
    
    NSLog(@"timer");
}


- (IBAction)actionPhotoQrCode:(id)sender
{
    [self.activityIndicator startAnimating];
    self.photoButton.hidden = YES;
    [self.delegate verifyWithPhoto];
}


- (IBAction)actionCancel:(id)sender
{
    //TODO: ask if user's is sure that want to cancel
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wait"
                                                    message:@"Are you sure you want to cancel this challenge?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            [self.delegate didOver];
            break;
        default:
            break;
    }
}

@end
