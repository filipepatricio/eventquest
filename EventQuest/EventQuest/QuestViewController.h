//
//  QuestViewController.h
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QuestModel;

@interface QuestViewController : UITableViewController
@property (strong, nonatomic) QuestModel *quest;
@property (strong, nonatomic) NSArray *challenges;
@end

