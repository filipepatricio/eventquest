//
//  InfoView.m
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "InfoView.h"
#import "ViewAnimationHelper.h"

@implementation InfoView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    [self.layer addAnimation:[ViewAnimationHelper appearAnimation] forKey:@"scale"];
}


@end
