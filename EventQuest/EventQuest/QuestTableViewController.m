//
//  QuestTableViewController.m
//  EventQuest
//
//  Created by Filipe Patrício on 22/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestTableViewController.h"
#import "QuestTableViewCell.h"
#import "QuestViewController.h"
#import "QuestModel.h"
#import "ChallengeModel.h"
#import "QuestStatusModel.h"
#import <Haneke/Haneke.h>
#import "StoreManagerHelper.h"

@interface QuestTableViewController ()
@property (strong, nonatomic) NSArray *quests; //Of QuestModel
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation QuestTableViewController


- (void)retreiveQuests {
    // GET RESULTS FROM PARSE IF ONLINE
    
    PFQuery *query = [PFQuery queryWithClassName:[QuestModel parseClassName]];
    [query fromLocalDatastore];
    [query findObjectsInBackgroundWithBlock:^(NSArray *quests, NSError *error) {
        
        self.quests = quests;

        PFQuery *queryOnline = [PFQuery queryWithClassName:[QuestModel parseClassName]];
        [queryOnline findObjectsInBackgroundWithBlock:^(NSArray *quests, NSError *error) {
            
            NSMutableArray *newQuests = [NSMutableArray arrayWithArray:self.quests];
            for(QuestModel *quest in quests)
            {
                BOOL questExists = NO;
                for(QuestModel *localQuest in self.quests)
                {
                    if([quest.name isEqualToString:localQuest.name])
                    {
                        questExists = YES;
                    }
                    
                }
                //save in local storage
                if(!questExists)
                {
                    [newQuests addObject:quest];
                    [quest pinInBackground];
                }
            }
            
            self.quests = newQuests.copy;
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
        }];
        
        
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
        
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:76.0/255.0
                                                     green:173.0/255.0
                                                      blue:240.0/255.0
                                                     alpha:1.0];

    self.refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 30)];
    //    self.refreshControl.backgroundColor = [UIColor purpleColor];
    //    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(retreiveQuests)
                  forControlEvents:UIControlEventValueChanged];
    

    //    [self createQuests];
    [self.activityIndicator startAnimating];
    [self retreiveQuests];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:76.0/255.0
                      green:173.0/255.0
                       blue:240.0/255.0
                      alpha:1.0],NSForegroundColorAttributeName,
      [UIFont fontWithName:@"RakoonPersonalUse" size:35.0], NSFontAttributeName, nil]
     ];
}



//Fill Parse with quests and challenges
-(void)createQuests
{
    NSMutableArray *newQuests = [NSMutableArray array];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Quests" ofType:@"plist"];
    NSDictionary *dictionaryWithQuests = [[NSDictionary alloc] initWithContentsOfFile:filePath];
    
    for(NSDictionary *questDict in dictionaryWithQuests[@"quests"])
    {
        QuestModel *quest = [QuestModel object];
        quest.name = questDict[@"name"];
        quest.info = questDict[@"info"];
        quest.sponsor = questDict[@"sponsor"];
        quest.imageURLString = questDict[@"imageURL"];
        quest.minimumChallengesRequiredToFinish = [questDict[@"minimumChallengesRequiredToFinish"] integerValue];
        quest.prize = questDict[@"prize"];
        quest.prizeDeliveryInstructions = questDict[@"prizeDeliveryInstructions"];
        
        [quest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if (succeeded) {
                
                NSMutableArray *newChallenges = [NSMutableArray array];
                for(NSDictionary *challengeDict in questDict[@"challenges"])
                {
                    ChallengeModel *challenge = [ChallengeModel object];
                    challenge.name = challengeDict[@"name"];
                    challenge.info = challengeDict[@"info"];
                    challenge.totalDuration = [challengeDict[@"totalDuration"] integerValue];
                    challenge.category = [challengeDict[@"category"] integerValue];
                    challenge.difficulty = [challengeDict[@"difficulty"] integerValue];
                    challenge.latitude = [challengeDict[@"latitude"] doubleValue];
                    challenge.longitude = [challengeDict[@"longitude"] doubleValue];
                    challenge.dateToExpire = challengeDict[@"dateToExpire"];
                    challenge.quest = quest;
                    [newChallenges addObject:challenge];
                }
                quest.challenges = [newChallenges copy];
                [quest saveInBackground];
                
            } else {
                // There was a problem, check error.description
            }
        }];
        [newQuests addObject:quest];
    }
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.quests.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QuestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuestCell" forIndexPath:indexPath];
    
    QuestModel *quest = self.quests[indexPath.row];
    
    cell.nameLabel.text = quest.name;
    cell.sponsorNameLabel.text = [NSString stringWithFormat:@"Sponsor: %@", quest.sponsor ];
    cell.prizeNameLabel.text = [NSString stringWithFormat:@"Prize: %@", quest.prize ];
    [cell.sponsorImageView hnk_setImageFromURL: [NSURL URLWithString:quest.imageURLString]];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    return indexPath;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.destinationViewController isKindOfClass:[QuestViewController class]])
    {
        QuestViewController *challengesVC = segue.destinationViewController;
        challengesVC.quest = (QuestModel*)self.quests[self.selectedIndexPath.row];
        challengesVC.challenges = ((QuestModel*)self.quests[self.selectedIndexPath.row]).challenges;
    }
}


@end
