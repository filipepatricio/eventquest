//
//  QuestStatusModel.m
//  EventQuest
//
//  Created by Filipe Patrício on 23/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestStatusModel.h"
#import "QuestModel.h"
#import "ChallengeStatusModel.h"

@implementation QuestStatusModel 

-(instancetype)initWithQuest:(QuestModel*)quest
{
    if(self = [self init])
    {
        self.name = quest.name;
        self.info = quest.info;
        self.sponsor = quest.sponsor;
        self.minimumChallengesRequiredToFinish = quest.minimumChallengesRequiredToFinish;
        self.imageURLString = quest.imageURLString;
        self.prize = quest.prize;

        
        PFQuery *query = [PFQuery queryWithClassName:[ChallengeModel parseClassName]];
        [query whereKey:@"quest" equalTo:quest];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSMutableArray *newChallengesStatus = [NSMutableArray array];
            for(ChallengeModel *challenge in objects)
            {
                ChallengeStatusModel *challengeStatus = [[ChallengeStatusModel alloc] initWithChallenge:(ChallengeModel*)challenge];
                [newChallengesStatus addObject:challengeStatus];

            }
            self.challengesStatus = newChallengesStatus.copy;
        }];

        
        
        
    }
    return self;
}

@end
