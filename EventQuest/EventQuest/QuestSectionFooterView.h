//
//  QuestSectionFooter.h
//  EventQuest
//
//  Created by Filipe Patrício on 19/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QuestSectionFooterView;

@protocol FooterViewDelegate <NSObject, NSCopying>
@required
-(void)didTapOnFooter:(QuestSectionFooterView*)footer;
@end

@interface QuestSectionFooterView : UIView
@property (weak, nonatomic) id<FooterViewDelegate>delegate;
@end
