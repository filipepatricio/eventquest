//
//  ChallengeModel.h
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "QuestModel.h"


@interface ChallengeModel : PFObject <PFSubclassing>

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *info;
@property (assign, nonatomic) NSTimeInterval totalDuration;     // Time to finish the challenge (in seconds)
@property (assign, nonatomic) NSUInteger category;         // Challenge category
@property (assign, nonatomic) NSUInteger difficulty;       // Easy, normal, hard
@property (strong, nonatomic) NSDate *dateToExpire;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (assign, nonatomic) QuestModel *quest;

+ (NSString *)parseClassName;

@end
