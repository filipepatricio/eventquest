//
//  ChallengeViewController.m
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "UIAlertView+Blocks.h"
#import "ChallengeViewController.h"
#import "InfoView.h"
#import "RequestView.h"
#import "StartedView.h"
#import "QRCodeView.h"
#import "MapViewController.h"
#import "NSDate+Utilities.h"


#import "ChallengeStatusModel.h"

//typedef enum : NSUInteger {
//    INFO_VIEW,
//    REQUEST_VIEW,
//    STARTED_VIEW,
////    QRCODE_VIEW,
//    ChallengeViewsEnumCount,
//} ChallengeViews;

typedef NS_ENUM(NSUInteger, ChallengeViewState) {
    ChallengeViewStateInfo,
    ChallengeViewStateRequest,
    ChallengeViewStateStarted,
    //    QRCODE_VIEW,
    ChallengeViewsEnumCount,
};

@interface ChallengeViewController () <ChallengeRequestDelegate,ChallengeStartedDelegate, QRCodeScannerDelegate>
@property (nonatomic, strong) NSArray *views;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *totalDurationLabel;

@end

@implementation ChallengeViewController

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    InfoView *infoView = [[[NSBundle mainBundle] loadNibNamed:@"InfoView" owner:self options:nil] objectAtIndex:0];
    RequestView *requestView = [[[NSBundle mainBundle] loadNibNamed:@"RequestView" owner:self options:nil] objectAtIndex:0];
    StartedView *startedView = [[[NSBundle mainBundle] loadNibNamed:@"StartedView" owner:self options:nil] objectAtIndex:0];
//    QRCodeView *qrcodeView = [[[NSBundle mainBundle] loadNibNamed:@"QRCodeView" owner:self options:nil] objectAtIndex:0];
    
    NSMutableArray *mutableViews = [NSMutableArray arrayWithCapacity:ChallengeViewsEnumCount];
    [mutableViews insertObject:infoView atIndex:ChallengeViewStateInfo];
    [mutableViews insertObject:requestView atIndex:ChallengeViewStateRequest];
    [mutableViews insertObject:startedView atIndex:ChallengeViewStateStarted];
//    [mutableViews insertObject:qrcodeView atIndex:QRCODE_VIEW];
    
    for(id view in mutableViews)
        if([view respondsToSelector:@selector(setDelegate:)])
            [view setDelegate:self];

    self.views = [mutableViews copy];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateContainerView];

    self.nameLabel.text = self.challengeStatus.name;
    NSDictionary *attributes = [(NSAttributedString *)self.infoLabel.attributedText attributesAtIndex:0 effectiveRange:NULL];
    self.infoLabel.attributedText = [[NSAttributedString alloc] initWithString:self.challengeStatus.info attributes:attributes];
    
    NSDictionary *timemap = [NSDate createTimemapForSeconds:self.challengeStatus.totalDuration];
    NSString *timeToFinishString = [NSString stringWithFormat:@"%02ld:%02ld:%02ld",
                           [timemap[@"h"] longValue],
                           [timemap[@"m"] longValue],
                           [timemap[@"s"] longValue]];
    self.totalDurationLabel.text = [self.totalDurationLabel.text stringByAppendingString:timeToFinishString];

}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [((StartedView*)self.views[ChallengeViewStateStarted]).timerToUpdateTimeLabel invalidate];
    ((StartedView*)self.views[ChallengeViewStateStarted]).timerToUpdateTimeLabel = nil;
}

-(UIView *)viewToPresent
{
    UIView *view;
    switch (self.challengeStatus.state) {
//        case LOCKED:
//            view = self.views[INFO_VIEW];
//            [self setInfoView:(InfoView*)view withStateDescription:@"Locked" withStateImageName:@"lock"];
//            break;
        case NEW:
            view = self.views[ChallengeViewStateRequest];
            break;
        case STARTED:
            view = self.views[ChallengeViewStateStarted];
            [self setStartedView:(StartedView*)view withFinishDate:self.challengeStatus.dateToFinish];
            break;
        case REFUSED:
            view = self.views[ChallengeViewStateInfo];
            [self setInfoView:(InfoView*)view withStateDescription:@"Refused" withStateImageName:@"sad"];
            break;
        case FINISHED:
            view = self.views[ChallengeViewStateInfo];
            [self setInfoView:(InfoView*)view withStateDescription:@"Finished" withStateImageName:@"happy"];
            break;
        case OVER:
            view = self.views[ChallengeViewStateInfo];
            [self setInfoView:(InfoView*)view withStateDescription:@"Over" withStateImageName:@"sad"];
            break;
        default:
            break;
    }
    return view;
}

-(void)setInfoView:(InfoView*)infoView withStateDescription:(NSString*)description withStateImageName:(NSString*)imageName
{
    infoView.stateDescriptionLabel.text = description;
    infoView.stateImageView.image = [UIImage imageNamed:imageName];
}

-(void)setStartedView:(StartedView*)startedView withFinishDate:(NSDate*)finishDate
{
    startedView.dateToFinish = finishDate;
}

-(void)updateContainerView
{
    //Remove all containerView subviews first
    NSArray *viewsToRemove = [self.containerView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    UIView *viewToPresent = [self viewToPresent];
    
    [self.containerView addSubview:viewToPresent];
    ((UIView*)viewToPresent).frame = self.containerView.bounds;
    
}

#pragma mark ChallengeRequestDelegate

- (void)changeChallengeToState:(NSUInteger)state
{
    // Change challenge state
    self.challengeStatus.state = state;
    [self.delegate didUpdateChallenge:self.challengeStatus];
    [self updateContainerView];
}

-(void)didAcceptChallenge:(BOOL)accepted
{

    if(accepted)
        [self changeChallengeToState:STARTED];
    else
        [self changeChallengeToState:REFUSED];
    
}

#pragma mark ChallengeStartedDelegate

-(void)verifyWithPhoto
{
    QRCodeView *qrcodeView = [[[NSBundle mainBundle] loadNibNamed:@"QRCodeView" owner:self options:nil] objectAtIndex:0];
    qrcodeView.delegate = self;
    [self.containerView addSubview:qrcodeView];

}

-(void)didOver
{
    [self changeChallengeToState:OVER];
}


#pragma mark QRCodeViewDelegate


-(void)qrcodeView:(QRCodeView*)qrcodeView didReadQRCodeWithString:(NSString *)detectionString
{
        NSLog(@"%@", detectionString);
        if([detectionString isEqualToString:self.challengeStatus.nameMD5])
        {
            //TODO: finish challenge;
            NSLog(@"%@", @"FINISHED");
            [qrcodeView removeFromSuperview];
            [self changeChallengeToState:FINISHED];
        }
        else
        {
            //TODO: try again;
            NSLog(@"%@", @"TRY AGAIN");
            UIAlertView *alert = [UIAlertView showWithTitle:NSLocalizedString(@"Wrong Code", nil)
                                                    message:@"You didn't find the correct code =( \nTry in another place."
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil
                                                   tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex)
                                  {
                                      [qrcodeView removeQrcodeView];
                                  }];
            [alert show];
        }
}

-(void)qrcodeView:(QRCodeView*)qrcodeView didCloseScanner:(BOOL)isScannerClosed
{
    StartedView *startedView = (StartedView*)[self.containerView.subviews firstObject];
    startedView.photoButton.hidden = NO;
    [startedView.activityIndicator stopAnimating];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        MapViewController *mapVC = segue.destinationViewController;
        mapVC.challengesStatus = @[self.challengeStatus];
    }
    
    
}



@end
