//
//  QuestViewController.m
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestViewController.h"
#import "QuestModel.h"
#import "ChallengeModel.h"
#import "ChallengeTableViewCell.h"
#import "ChallengeViewController.h"
#import "StoreManagerHelper.h"
#import "NSTimer+Block.h"
#import "QuestSectionFooterView.h"
#import "QuestSectionHeaderView.h"
#import "KLCPopup/KLCPopup.h"
#import "QR/UIImage+MDQRCode.h"
#import "MapViewController.h"
#import "PrizeView.h"

@interface QuestViewController () <UpdateChallengeDelegate, FooterViewDelegate>
@property (strong, nonatomic) NSArray *challengesStatus; //Of ChallengesStatus
@property (strong, nonatomic) NSMutableArray *timersToOverChallenges; //Of NSTimer
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (assign, nonatomic) CGFloat footerHeight;
@property (strong, nonatomic) QuestSectionFooterView *footerView;

@property (assign, nonatomic) CGFloat headerHeight;
@property (strong, nonatomic) QuestSectionHeaderView *headerView;

@end

@implementation QuestViewController


-(NSArray *)challengesStatus
{
    if(!_challengesStatus)
    {
        _challengesStatus = [StoreManagerHelper loadDataFromKey:self.quest.name];
        
        for(ChallengeStatusModel *challengeStatus in _challengesStatus)
        {
            [challengeStatus checkIfIsOverAndChangeStateIfTrue];
        }
    }
    return _challengesStatus;
}

-(NSMutableArray *)timersToOverChallenges
{
    if(!_timersToOverChallenges)
    {
        _timersToOverChallenges = [NSMutableArray array];
    }
    return _timersToOverChallenges;
}


//- (void)retreiveChallenges {
//
//    NSMutableArray *newChallengesStatus = [NSMutableArray arrayWithArray:self.challengesStatus];
//    
//    for(ChallengeModel *challenge in self.challenges)
//    {
//        BOOL challengeExists = NO;
//        for(ChallengeStatusModel *challengeStatus in self.challengesStatus)
//        {
//            if([challengeStatus.name isEqualToString:challenge.name])
//            {
//                challengeExists = YES;
//            }
//            
//        }
//        if(!challengeExists)
//        {
//            //Create new challengeStatus
//            ChallengeStatusModel *newChallengeStatus = [[ChallengeStatusModel alloc] initWithChallenge:challenge];
//            [newChallengesStatus addObject:newChallengeStatus];
//        }
//    }
//    
//    self.challengesStatus = newChallengesStatus.copy;
//    
//    [StoreManagerHelper saveData:self.challengesStatus];
//    
//    [self.refreshControl endRefreshing];
//    [self.tableView reloadData];
//    
//    
//}

- (void)retreiveChallengesFromParse {
    // GET RESULTS FROM PARSE IF ONLINE
    PFQuery *query = [PFQuery queryWithClassName:[ChallengeModel parseClassName]];
    [query whereKey:@"quest" equalTo:self.quest];
    [query findObjectsInBackgroundWithBlock:^(NSArray *challenges, NSError *error) {
        
        NSMutableArray *newChallengesStatus = [NSMutableArray arrayWithArray:self.challengesStatus];

        for(ChallengeModel *challenge in challenges)
        {
            BOOL challengeExists = NO;
            for(ChallengeStatusModel *challengeStatus in self.challengesStatus)
            {
                if([challengeStatus.name isEqualToString:challenge.name])
                {
                    challengeExists = YES;
                }
                
            }
            if(!challengeExists)
            {
                //Create new challengeStatus
                ChallengeStatusModel *newChallengeStatus = [[ChallengeStatusModel alloc] initWithChallenge:challenge];
                [newChallengesStatus addObject:newChallengeStatus];
            }
        }
        
        self.challengesStatus = newChallengesStatus.copy;
        
        [StoreManagerHelper saveData:self.challengesStatus withKey:self.quest.name];
        
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];

    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:76.0/255.0
                                                     green:173.0/255.0
                                                      blue:240.0/255.0
                                                     alpha:1.0];
 
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
//    self.refreshControl.backgroundColor = [UIColor purpleColor];
//    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(retreiveChallengesFromParse)
                  forControlEvents:UIControlEventValueChanged];
    
    [self retreiveChallengesFromParse];
    [self checkIfMinimumChallengesRequiredAreFinished];
    
    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"QuestSectionHeaderView" owner:self options:nil] objectAtIndex:0];
    self.headerView.sponsorNameLabel.text = self.quest.sponsor;
    self.headerView.prizeNameLabel.text = self.quest.prize;
    self.headerView.minimumNumberLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)self.quest.minimumChallengesRequiredToFinish];
    
    self.navigationItem.title = self.quest.name;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor],NSForegroundColorAttributeName,
      [UIFont fontWithName:@"STHeitiSC-Medium" size:18.0], NSFontAttributeName, nil]
     ];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.observationInfo)
        [self removeObserver:self forKeyPath:@"challengesStatus"];
    [self addObserver:self forKeyPath:@"challengesStatus" options:0 context:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if(self.observationInfo)
        [self removeObserver:self forKeyPath:@"challengesStatus"];
}

- (void)beginRefreshingTableView {
    
    [self.refreshControl beginRefreshing];
    
    if (self.tableView.contentOffset.y <= 0) {
        
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){
            
            self.tableView.contentOffset = CGPointMake(0, -self.refreshControl.frame.size.height);
            
        } completion:^(BOOL finished){
            
        }];
        
    }
}
 
- (IBAction)actionResetChallengesStatus:(id)sender
{
    for(ChallengeStatusModel *challengesStatus in self.challengesStatus)
        challengesStatus.state = NEW;
    
    [StoreManagerHelper saveData:self.challengesStatus withKey:self.quest.name];
    [self checkIfMinimumChallengesRequiredAreFinished];
    [self.tableView reloadData];
    
}

#pragma mark UITableViewDataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.challengesStatus.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChallengeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChallengeCell" forIndexPath:indexPath];
    
    ChallengeStatusModel *challengeStatus = self.challengesStatus[indexPath.row];
    cell.nameLabel.text = challengeStatus.name;
    cell.stateLabel.text = [challengeStatus stateDescription];
    cell.stateImageView.image = [UIImage imageNamed:[challengeStatus stateImageName]];
    
    return cell;
}

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    return indexPath;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return self.footerView;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.quest.minimumChallengesRequiredToFinish > 0 ? [NSString stringWithFormat:@"Minimum challenges required: %d", self.quest.minimumChallengesRequiredToFinish] : @"Minimum challenges required: All";
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
//    [header.textLabel setFont:[UIFont fontWithName:header.textLabel.font.familyName size:16]];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Header"];
    headerView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8];
    
    return headerView;
}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return self.headerView.frame.size.height;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return self.footerHeight;
}

#pragma mark UINavigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[ChallengeViewController class]])
    {
        ChallengeViewController *challengeVC = segue.destinationViewController;
        challengeVC.delegate = self;
        challengeVC.challengeStatus = self.challengesStatus[self.selectedIndexPath.row];
    }
    else if([segue.destinationViewController isKindOfClass:[MapViewController class]])
    {
        MapViewController *mapVC = segue.destinationViewController;
        mapVC.challengesStatus = self.challengesStatus;
    }
}

#pragma mark UpdateChallengeDelegate

-(void)didUpdateChallenge:(ChallengeStatusModel*)challengeStatus
{
    NSUInteger rowToReload = [self.challengesStatus indexOfObject:challengeStatus];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowToReload inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:YES];
    [StoreManagerHelper saveData:self.challengesStatus withKey:self.quest.name];
    
    [self checkIfMinimumChallengesRequiredAreFinished];
    [self checkIfSomeEventStartedAndAddTimerToBeOver];
}

-(void)didTapOnFooter:(QuestSectionFooterView *)footer
{
    PrizeView *prizeView = [[[NSBundle mainBundle] loadNibNamed:@"PrizeView" owner:self options:nil] objectAtIndex:0];
    
    UIImage *qrcodeImage = [UIImage mdQRCodeForString:self.quest.prize size:100];
    prizeView.qrcodeImageView.image = qrcodeImage;
    prizeView.prizeNameLabel.text = self.quest.prize;
    prizeView.instructionsLabel.text = self.quest.prizeDeliveryInstructions;
//    UIImageView *qrcodeImageView = [[UIImageView alloc] initWithImage:qrcodeImage];
//    qrcodeImageView.backgroundColor = [UIColor whiteColor];
//    qrcodeImageView.image = qrcodeImage;
//    qrcodeImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    KLCPopup *popup = [KLCPopup popupWithContentView:prizeView
                          showType:KLCPopupShowTypeGrowIn
                       dismissType:KLCPopupDismissTypeShrinkOut
                          maskType:KLCPopupMaskTypeDimmed
          dismissOnBackgroundTouch:YES
             dismissOnContentTouch:YES
     ];
    [popup show];
}


-(BOOL)checkIfMinimumChallengesRequiredAreFinished
{
    NSUInteger challengesAlreadyFinished = 0;
    for(ChallengeStatusModel *challengeStatus in self.challengesStatus)
    {
        if(challengeStatus.state == FINISHED)
        {
            challengesAlreadyFinished++;
        }
    }
    
    //self.quest.minimumChallengesRequiredToFinish means that all challenges has to be finished
    BOOL isAllRequiredChallengesFinished = (self.quest.minimumChallengesRequiredToFinish > 0 && challengesAlreadyFinished >= self.quest.minimumChallengesRequiredToFinish) || challengesAlreadyFinished == self.quest.challenges.count;
    
    if(isAllRequiredChallengesFinished) //Change footer
    {
        [self.tableView beginUpdates];
        self.footerView = [[[NSBundle mainBundle] loadNibNamed:@"QuestSectionFooterView" owner:self options:nil] objectAtIndex:0];
        self.footerHeight = self.footerView.frame.size.height;
        self.footerView.delegate = self;
        [self.tableView endUpdates];
    }
    else
    {
//        [self.tableView beginUpdates];
        self.footerView = nil;
        self.footerHeight = 1.0;
//        [self.tableView endUpdates];
    }
    
    
    return isAllRequiredChallengesFinished;
}

#warning Improve to One Timer with array of ongoing challenges!
-(void)checkIfSomeEventStartedAndAddTimerToBeOver
{

    for(ChallengeStatusModel *challengeStatus in self.challengesStatus)
    {
        NSUInteger rowToReload = [self.challengesStatus indexOfObject:challengeStatus];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowToReload inSection:0];
        if(challengeStatus.state == STARTED)
        {
            NSTimer *timerToChallengeBeOver = [NSTimer scheduledTimerWithTimeInterval:challengeStatus.timeLeft repeats:NO block:^{
                [challengeStatus checkIfIsOverAndChangeStateIfTrue];
                if(challengeStatus.state == OVER)
                {
                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:YES];
                    [StoreManagerHelper saveData:self.challengesStatus withKey:self.quest.name];
                }
            }];
            [self.timersToOverChallenges addObject:timerToChallengeBeOver];
        }
    }
}



- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"challengesStatus"])
    {
        [self checkIfMinimumChallengesRequiredAreFinished];
        [self checkIfSomeEventStartedAndAddTimerToBeOver];
    }
}



@end
