//
//  QuestTableViewCell.h
//  EventQuest
//
//  Created by Filipe Patrício on 22/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sponsorImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sponsorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *prizeNameLabel;
@end
