//
//  StoreManagerHelper.m
//  EventQuest
//
//  Created by Filipe Patrício on 18/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "StoreManagerHelper.h"

#define kDataKey        @"Data"
#define kDataFile       @"data.plist"

@interface StoreManagerHelper()

@end

@implementation StoreManagerHelper

+(NSArray*)loadDataFromKey:(NSString*)key
{
    NSString *dataPath = [[StoreManagerHelper applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", key, kDataFile]];
    
    NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];
    if(!codedData)
        return nil;
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
    NSArray *challengesData = [unarchiver decodeObjectForKey:kDataKey];
    [unarchiver finishDecoding];
    
    return challengesData;
}

+(void)saveData:(NSArray*)dataArray withKey:(NSString*)key
{
    NSString *dataPath = [[StoreManagerHelper applicationDocumentsDirectory].path
                          stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", key, kDataFile]];
    NSMutableData *mutableData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mutableData];
    [archiver encodeObject:dataArray forKey:kDataKey];
    [archiver finishEncoding];
    [mutableData writeToFile:dataPath atomically:YES];
    
}

+ (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

@end
