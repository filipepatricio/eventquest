//
//  ChallengeViewController.h
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChallengeStatusModel;

@protocol UpdateChallengeDelegate <NSObject>
@required
-(void)didUpdateChallenge:(ChallengeStatusModel*)challengeStatus;
@end

@interface ChallengeViewController : UIViewController

@property (weak, nonatomic) id<UpdateChallengeDelegate> delegate;
@property (strong, nonatomic) ChallengeStatusModel *challengeStatus;

@end



