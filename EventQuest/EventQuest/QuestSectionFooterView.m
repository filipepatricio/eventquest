//
//  QuestSectionFooter.m
//  EventQuest
//
//  Created by Filipe Patrício on 19/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestSectionFooterView.h"

@implementation QuestSectionFooterView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
    

}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.delegate didTapOnFooter:self];
}


@end
