//
//  MapViewController.h
//  EventQuest
//
//  Created by Filipe Patrício on 22/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ChallengeStatusModel.h"

@interface MapViewController : UIViewController
@property (strong, nonatomic) NSArray *challengesStatus; //Of ChallengeStatusModel
@end
