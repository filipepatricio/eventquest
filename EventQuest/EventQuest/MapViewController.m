//
//  MapViewController.m
//  EventQuest
//
//  Created by Filipe Patrício on 22/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "MapViewController.h"
#import <CoreLocation/CoreLocation.h>

#define MAP_DISTANCE_CONSTANT 1000.0;

@interface MapViewController () <CLLocationManagerDelegate, MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.locationManager = [[CLLocationManager alloc] init];
    if([CLLocationManager locationServicesEnabled])
    {
        if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [self.locationManager requestWhenInUseAuthorization];
            //Add to Info.plist: NSLocationWhenInUseUsageDescription
        }
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        
        self.map.delegate = self;
        self.map.showsUserLocation = YES;
        [self.map addAnnotations:self.challengesStatus];
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.locationManager stopUpdatingLocation];
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    [self actionCenter:nil];
}

- (IBAction)actionCenter:(id)sender
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.map.userLocation.coordinate, 1000.0f, 1000.0f);
    
    MKCoordinateRegion adjustedRegion = [self.map regionThatFits:region];
    [self.map setRegion:adjustedRegion animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
