//
//  ChallengeStatusModel.m
//  EventQuest
//
//  Created by Filipe Patrício on 18/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "ChallengeStatusModel.h"
#import "NSString+MD5.h"
#import "NSDate+Utilities.h"

@implementation ChallengeStatusModel

- (instancetype)initWithChallenge:(ChallengeModel*)challenge;
{
    if(self = [self init])
    {
        self.name = challenge.name;
        self.info = challenge.info;
        self.totalDuration = challenge.totalDuration;
        self.difficulty = challenge.difficulty;
        self.category = challenge.category;
        self.dateToExpire = challenge.dateToExpire;
        self.latitude = challenge.latitude;
        self.longitude = challenge.longitude;
        self.state = NEW;
    }
    return self;
}

-(void)setState:(NSUInteger)state
{
    _state = state;
    
    if(state == NEW)
    {
        self.startDate = nil;
        self.finishDate = nil;
        self.dateToFinish = nil;
    }
    else if(state == STARTED)
    {
        if(self.startDate == nil)
        {
            self.startDate = [NSDate date];
            self.dateToFinish = [self.startDate dateByAddingTimeInterval:self.totalDuration];
        }
    }
    else if(state == FINISHED)
    {
        if(self.finishDate == nil)
        {
            self.finishDate = [NSDate date];
        }
    }
}

-(NSString *)stateDescription
{
    NSString *stateDescription;
    switch (self.state) {
//        case LOCKED:
//            stateDescription = @"Locked";
//            break;
        case NEW:
            stateDescription = @"New Challenge";
            break;
        case STARTED:
            stateDescription = @"On going";
            break;
        case REFUSED:
            stateDescription = @"Refused";
            break;
        case FINISHED:
            stateDescription = @"Finished";
            break;
        case OVER:
            stateDescription = @"Over";
            break;
        default:
            break;
    }
    return stateDescription;
}

-(NSString *)stateImageName
{
    NSString *stateImageName;
    switch (self.state) {
//        case LOCKED:
//            stateImageName = @"lock";
//            break;
        case NEW:
            stateImageName = @"star";
            break;
        case STARTED:
            stateImageName = @"ongoing";
            break;
        case REFUSED:
            stateImageName = @"failed";
            break;
        case FINISHED:
            stateImageName = @"success";
            break;
        case OVER:
            stateImageName = @"failed";
            break;
        default:
            break;
    }
    return stateImageName;
}

- (NSTimeInterval)timeLeft
{
    NSDate *now = [NSDate date];
    NSTimeInterval timeLeft = [self.dateToFinish timeIntervalSinceDate:now];
    return timeLeft;
}

-(BOOL)isOver
{
    return self.timeLeft <= 0;
}

-(void)checkIfIsOverAndChangeStateIfTrue
{
    if(self.state == STARTED && self.isOver)
        self.state = OVER;
}

-(NSString*)nameMD5
{
    return self.name.md5;
}

-(CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coord;
    coord.latitude = self.latitude;
    coord.longitude = self.longitude;
    return coord;
}

-(NSString *)title
{
    return self.name;
}

//-(NSString *)subtitle
//{
//    return [NSString stringWithFormat:@"%d", self.type];
//}

@end
