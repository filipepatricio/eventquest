//
//  NSString+MD5.h
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//  http://stackoverflow.com/questions/1524604/md5-algorithm-in-objective-c

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString *)md5;
@end
