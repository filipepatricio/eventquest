//
//  StoreManagerHelper.h
//  EventQuest
//
//  Created by Filipe Patrício on 18/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreManagerHelper : NSObject
+(NSArray*)loadDataFromKey:(NSString*)key;
+(void)saveData:(NSArray*)dataArray withKey:(NSString*)key;
@end
