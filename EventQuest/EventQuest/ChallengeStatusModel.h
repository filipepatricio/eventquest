//
//  ChallengeStatusModel.h
//  EventQuest
//
//  Created by Filipe Patrício on 18/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "ChallengeModel.h"
#import <MapKit/MapKit.h>

typedef enum : NSUInteger {
//    LOCKED,
    NEW,
    STARTED,
    REFUSED,
    FINISHED,
    OVER,
} ChallengeState;

@interface ChallengeStatusModel : MTLModel <MKAnnotation>

//ChallengeModel Properties
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *info;
@property (assign, nonatomic) NSTimeInterval totalDuration;     // Time to finish the challenge (in seconds)
@property (assign, nonatomic) NSUInteger category;         // Challenge category
@property (assign, nonatomic) NSUInteger difficulty;       // Easy, normal, hard
@property (strong, nonatomic) NSDate *dateToExpire;

@property (assign, nonatomic) NSUInteger state;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *dateToFinish;
@property (strong, nonatomic) NSDate *finishDate;

@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;

//@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

-(instancetype)initWithChallenge:(ChallengeModel*)challenge;
-(NSString *)stateDescription;
-(NSString *)stateImageName;
-(NSString *)nameMD5;
-(NSTimeInterval)timeLeft;
-(void)checkIfIsOverAndChangeStateIfTrue;
-(CLLocationCoordinate2D)coordinate;
-(NSString *)title;

@end
