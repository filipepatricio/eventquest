//
//  InfoView.h
//  EventQuest
//
//  Created by Filipe Patrício on 17/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;
@property (weak, nonatomic) IBOutlet UILabel *stateDescriptionLabel;
@end
