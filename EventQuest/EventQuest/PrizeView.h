//
//  PrizeView.h
//  EventQuest
//
//  Created by Filipe Patrício on 24/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrizeView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *qrcodeImageView;
@property (weak, nonatomic) IBOutlet UILabel *prizeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@end
