//
//  ChallengeModel.m
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "ChallengeModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation ChallengeModel

@dynamic name;
@dynamic info;
@dynamic totalDuration;
@dynamic category;
@dynamic difficulty;
@dynamic dateToExpire;
@dynamic latitude;
@dynamic longitude;
@dynamic quest;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Challenge";
}


@end
