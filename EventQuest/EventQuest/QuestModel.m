//
//  QuestModel.m
//  EventQuest
//
//  Created by Filipe Patrício on 16/06/15.
//  Copyright (c) 2015 Filipe Patrício. All rights reserved.
//

#import "QuestModel.h"

@interface QuestModel ()

@end

@implementation QuestModel

@dynamic name;
@dynamic info;
@dynamic sponsor;
@dynamic imageURLString;
@dynamic minimumChallengesRequiredToFinish;
@dynamic prize;
@dynamic prizeDeliveryInstructions;
@dynamic challenges;


+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Quest";
}

@end
